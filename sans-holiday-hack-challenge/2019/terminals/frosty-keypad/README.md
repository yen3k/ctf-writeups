## Frosty Keypad

### Description

```
Hey kid, it's me, Tangle Coalbox.

I'm sleuthing again, and I could use your help.

Ya see, this here number lock's been popped by someone.

I think I know who, but it'd sure be great if you could open this up for me.

I've got a few clues for you.

	1. One digit is repeated once.

	2. The code is a prime number.

	3. You can probably tell by looking at the keypad which buttons are used.
```

### Solution

Found a python script to print
[prime numbers](https://www.programiz.com/python-programming/examples/prime-number-intervals).
Modified it a bit so it only prints prime numbers consisting only of 1's, 3's
and 7s, and which consists of 4 digits.

7331. Should have guessed that.
