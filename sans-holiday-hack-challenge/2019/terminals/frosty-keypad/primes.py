# Modified version of:
# https://www.programiz.com/python-programming/examples/prime-number-intervals

import re

lower = 0
upper = 9999

primes = []
for num in range(lower, upper + 1):
   # all prime numbers are greater than 1
   if num > 1:
       for i in range(2, num):
           if (num % i) == 0:
               break
       else:
           if bool(re.match('^[137]+$', str(num))) and len(str(num)) == 4:
                   primes.append(num)

print(primes)
