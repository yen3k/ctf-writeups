## Nyanshell

### Description

```
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄░░░░░░░░░
░░░░░░░░▄▀░░░░░░░░░░░░▄░░░░░░░▀▄░░░░░░░
░░░░░░░░█░░▄░░░░▄░░░░░░░░░░░░░░█░░░░░░░
░░░░░░░░█░░░░░░░░░░░░▄█▄▄░░▄░░░█░▄▄▄░░░
░▄▄▄▄▄░░█░░░░░░▀░░░░▀█░░▀▄░░░░░█▀▀░██░░
░██▄▀██▄█░░░▄░░░░░░░██░░░░▀▀▀▀▀░░░░██░░
░░▀██▄▀██░░░░░░░░▀░██▀░░░░░░░░░░░░░▀██░
░░░░▀████░▀░░░░▄░░░██░░░▄█░░░░▄░▄█░░██░
░░░░░░░▀█░░░░▄░░░░░██░░░░▄░░░▄░░▄░░░██░
░░░░░░░▄█▄░░░░░░░░░░░▀▄░░▀▀▀▀▀▀▀▀░░▄▀░░
░░░░░░█▀▀█████████▀▀▀▀████████████▀░░░░
░░░░░░████▀░░███▀░░░░░░▀███░░▀██▀░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
nyancat, nyancat
I love that nyancat!
My shell's stuffed inside one
Whatcha' think about that?
Sadly now, the day's gone
Things to do!  Without one...
I'll miss that nyancat
Run commands, win, and done!
Log in as the user alabaster_snowball with a password of Password2, and land in a Bash
 prompt.
Target Credentials:
username: alabaster_snowball
password: Password2
```

### Solution

Tried to change user:

```
su - alabaster_snowball
```

Only get nyaned if supplying `Password2` which tells that it is probably  the
right password.

Looking at `/etc/passwd` it says that the shell for the user
`alabaster_snowball` is `/bin/nsh` which is probably the nyan shell.

We are able to run `chattr` as root. We can see that by typing:

```
sudo -l
```

We do not have permission to just replace `/bin/nsh` with a copy of `/bin/bash`
because the `/bin/nsh` is immutable. We can see that by running:

```
lsattr /bin/nsh
```

It shows that the file has the `i` attribute which means it is immutable.
Removing this attribute with

```
chattr -i /bin/nsh
```

allows one to modify it and we can replace `/bin/nsh` with a copy of `/bin/bash`

```
cp /bin/bash /bin/nsh
```
