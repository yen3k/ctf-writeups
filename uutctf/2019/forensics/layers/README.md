## Layers (25)

### Description

*Layers are here*

**Files:**
* [Layers.jpg](Layers.jpg)

![Layers.jpg](Layers.jpg)

---

### Solution

Running `exiftool Layers.jpg` reveals that the flag is hidden in a text layer:

```
History Software Agent          : Adobe Photoshop CC 2015 (Windows), Adobe Photoshop CC 2015 (Windows)
History Changed                 : /
Text Layer Name                 : uutctf{can_you_see_me}
Text Layer Text                 : uutctf{can_you_see_me}
Profile CMM Type                : Linotronic
Profile Version                 : 2.1.0

```


